import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class MyTimer extends JFrame {
    private JPanel mainPanel;
    private JButton shutdownButton;
    private JButton gyberButton;
    private JButton cancelButton;
    private JTextField countMinutes;
    private JLabel textLabel;

    String command;
    int time;

    Timer timer;

    public MyTimer() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);

        shutdownButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                command = "shutdown /s";
                createTimer();
            }
        });
        gyberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                command = "shutdown /h";
                createTimer();
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                timer.stop();
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("MyTimer");
        frame.setContentPane(new MyTimer().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    public void createTimer() {

        try {
            time = Integer.parseInt(countMinutes.getText()) * 60 * 1000;
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Введите целое число!");
            return;
        }

        timer = new Timer(time, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Runtime runtime = Runtime.getRuntime();
                try {
                    Process proc = runtime.exec(command);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                System.exit(0);
            }
        });
        timer.start();
        timer.setRepeats(false);

    }
}
